/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const MyScmContract = require('./lib/my-scm-contract');

module.exports.MyScmContract = MyScmContract;
module.exports.contracts = [ MyScmContract ];
