/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');
const shim = require('fabric-shim');


class MyScmContract extends Contract {

//***********Item Creation and Manipulation**************/

    async itemExists(ctx, ItemId) {
        const buffer = await ctx.stub.getState(ItemId);
        return (!!buffer && buffer.length > 0);
    }
    async createItem (ctx, ItemId, Desc, UnitMeasure, UnitPrice) {
        let logger = shim.newLogger('Chaincode --> ');
        let CID = new shim.ClientIdentity(ctx.stub);
        let mspID = CID.getMSPID();
        logger.info('MSPID : '+ mspID);

        if(mspID == 'ManufacturerMSP'){
            const exists = await this.itemExists(ctx, ItemId);
            if (exists) {
                throw new Error(`The Item ${ItemId} already exists`);
            }
            const asset = {
                Desc: Desc,
                UnitMeasure: UnitMeasure,
                UnitPrice: Number(UnitPrice),
                Status: "Not Allocated",
                Owner: mspID,
                AssetCategory: "Product",
                PoNbr: "Not Assigned"
            };
            const buffer = Buffer.from(JSON.stringify(asset));
            await ctx.stub.putState(ItemId, buffer);

            let addItemEvent = {Type: 'item creation', Desc: Desc};
            await ctx.stub.setEvent('addItemEvent', Buffer.from(JSON.stringify(addItemEvent)));        
        }
        else{
            logger.info('Users under the following MSP : '+
                        mspID + 'cannot perform this action');
            return('Users under the following MSP : '+
            mspID + 'cannot perform this action');
        }
    }

    async readItem(ctx, ItemId) {
        const exists = await this.itemExists(ctx, ItemId);
        if (!exists) {
            throw new Error(`The Item ${ItemId} does not exist`);
        }
        const buffer = await ctx.stub.getState(ItemId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    async getItemOwner(ctx, ItemId) {
        const exists = await this.itemExists(ctx, ItemId);
        if (!exists) {
            throw new Error(`The Item ${ItemId} does not exist`);
        }
        const buffer = await ctx.stub.getState(ItemId);
        const asset = JSON.parse(buffer.toString());
        return asset.Owner;
    }

    async getItemCost(ctx, ItemId) {
        const exists = await this.itemExists(ctx, ItemId);
        if (!exists) {
            throw new Error(`The Item ${ItemId} does not exist`);
        }
        const buffer = await ctx.stub.getState(ItemId);
        const asset = JSON.parse(buffer.toString());
        return Number(asset.UnitPrice);
    }

    async updateItem(ctx, ItemID, status, poNbr) {
        const exists = await this.itemExists(ctx, ItemID);
        if (!exists) {
            throw new Error(`The Item ${ItemID} does not exist`);
        }

        const itemBuffer = await ctx.stub.getState(ItemID);
        const itemDetail = JSON.parse(itemBuffer.toString());
        itemDetail.Status = status;
        itemDetail.PoNbr = poNbr;

        const NewItembuffer = Buffer.from(JSON.stringify(itemDetail));
        await ctx.stub.putState(ItemID, NewItembuffer);

    }

    async updateItemOwner(ctx, ItemID, poOwner) {
        const exists = await this.itemExists(ctx, ItemID);
        if (!exists) {
            throw new Error(`The Item ${ItemID} does not exist`);
        }

        const itemBuffer = await ctx.stub.getState(ItemID);
        const itemDetail = JSON.parse(itemBuffer.toString());
        itemDetail.Status = "Confirmed";
        itemDetail.Owner = poOwner;

        const NewItembuffer = Buffer.from(JSON.stringify(itemDetail));
        await ctx.stub.putState(ItemID, NewItembuffer);

    }

    async deleteItem(ctx, ItemID) {
        let logger = shim.newLogger('Chaincode --> ');
        let CID = new shim.ClientIdentity(ctx.stub);
        let mspID = CID.getMSPID();
        logger.info('MSPID : '+ mspID);

        if(mspID == 'ManufacturerMSP'){
            const exists = await this.itemExists(ctx, ItemID);
            if (!exists) {
                throw new Error(`The Item ${ItemID} does not exist`);
            }
            await ctx.stub.deleteState(ItemID);
        }
        else{
            logger.info('Users under the following MSP : '+
                        mspID + 'cannot perform this action');
            return('Users under the following MSP : '+
            mspID + 'cannot perform this action');
        }
    }
 // ================== ORDER CREATION ====================== //

    async ordExists(ctx, ordNum) {
        const buffer = await ctx.stub.getState(ordNum);
        return (!!buffer && buffer.length > 0);
    }

    //Function to create order ************
    //async raiseOrder(ctx, ordNum, PoFromId, PoToId) {
    async raiseOrder(ctx, ordNum ){    
        let logger = shim.newLogger('Chaincode --> ');
        let CID = new shim.ClientIdentity(ctx.stub);
        let mspID = CID.getMSPID();
        logger.info('MSPID : '+ mspID);
        let PoFromId = mspID;
        let PoToId = "";

        if(mspID != 'ManufacturerMSP'){
            const exists = await this.ordExists(ctx, ordNum);
            if (exists) {
                throw new Error(`An open order with the same number:${ordNum} already exists`);
            }
            if (mspID == 'DistributorMSP') {
                const asset = {
                    ordNum: ordNum,
                    PoFromId: "DistributorMSP",
                    PoToId: "ManufacturerMSP",
                    PoStatus: "Created",
                    AssetCategory: "Order",
                    Items: [],
                    PoCost: 0
                };
    
                const buffer = Buffer.from(JSON.stringify(asset));
                await ctx.stub.putState(ordNum, buffer);
            }
            if (mspID == 'RetailerMSP') {
                const asset = {
                    ordNum: ordNum,
                    PoFromId: "RetailerMSP",
                    PoToId: "DistributorMSP",
                    PoStatus: "Initial",
                    Items: [],
                    PoCost: 0
                };
    
                const buffer = Buffer.from(JSON.stringify(asset));
                await ctx.stub.putState(ordNum, buffer);

            }

        }
        else{
            logger.info('Users under the following MSP : '+
                        mspID + 'cannot perform this action');
            return('Users under the following MSP : '+
            mspID + 'cannot perform this action');
        }
    }
    
    async readOrder(ctx, ordNum) {
        const exists = await this.ordExists(ctx, ordNum);
        if (!exists) {
            throw new Error(`An Order with: ${ordNum} does not exists`);
        }

        const buffer = await ctx.stub.getState(ordNum);
        const orderDetails = JSON.parse(buffer.toString());
        return orderDetails;
    }

    async addItemToOrder(ctx, ordNum, itemList) {
       // var poItemCost = 0;
       let logger = shim.newLogger('Chaincode --> ');
       let CID = new shim.ClientIdentity(ctx.stub);
       let mspID = CID.getMSPID();
       logger.info('MSPID : '+ mspID);

        const PoExists = await this.ordExists(ctx, ordNum);
        if (!PoExists) {
            throw new Error(`An open order with the Order number:${ordNum} doesnot exists`);
        }
        
        const PoItemExists = await this.itemExists(ctx, itemList);
        if (!PoItemExists) {
            throw new Error(`The Item ${itemList} does not exist`);
        }
    
        const poItemCost = await this.getItemCost(ctx, itemList);
    
        const orderBuffer = await ctx.stub.getState(ordNum);
        const ordDetail = JSON.parse(orderBuffer.toString());
                
        if(ordDetail.PoFromId == mspID){
            const newCost = Number(poItemCost) + Number(ordDetail.PoCost);

            ordDetail.Items.push(itemList);
            ordDetail.PoCost = newCost;

            const newBuffer = Buffer.from(JSON.stringify(ordDetail));
            await ctx.stub.putState(ordNum, newBuffer);
        }
        else{
            logger.info('Users under the following MSP : '+
                        mspID + 'cannot perform this action');
            return('Users under the following MSP : '+
            mspID + 'cannot perform this action');
        }
    }

    async getPoItems(ctx, ordNum) {
        const exists = await this.ordExists(ctx, ordNum);
        if (!exists) {
            throw new Error(`An Order with: ${ordNum} does not exists`);
        }

        const buffer = await ctx.stub.getState(ordNum);
        const orderDetails = JSON.parse(buffer.toString());
        const poItems = orderDetails.Items;
        return poItems;
    }
    
    async getPoOwner(ctx, ordNum) {
        const exists = await this.ordExists(ctx, ordNum);
        if (!exists) {
            throw new Error(`The Order ${ordNum} does not exist`);
        }
        const buffer = await ctx.stub.getState(ordNum);
        const asset = JSON.parse(buffer.toString());
        return asset.PoFromId;
    }
    
    // After the items are added to the order, the order is ready to be initaited for further processing

    async initiateOrder(ctx, ordNum) {
        // var poItemCost = 0;
        let logger = shim.newLogger('Chaincode --> ');
        let CID = new shim.ClientIdentity(ctx.stub);
        let mspID = CID.getMSPID();
        logger.info('MSPID : '+ mspID);

         let item = "";
         let index = 0;
         const PoExists = await this.ordExists(ctx, ordNum);
         if (!PoExists) {
             throw new Error(`An open order with the Order number:${ordNum} doesnot exists`);
         }
        
         let orderBuffer = await ctx.stub.getState(ordNum);
         let ordDetail = JSON.parse(orderBuffer.toString()); 
        
         // Change Status of the Order *****
         if(ordDetail.PoFromId == mspID){ 
            ordDetail.PoStatus = "Initiated";
        
            const newBuffer = Buffer.from(JSON.stringify(ordDetail));
            await ctx.stub.putState(ordNum, newBuffer);
         }
         
         //Allocate Item to this Order ****
         if(ordDetail.PoFromId == mspID){         
            const itemList = await this.getPoItems(ctx, ordNum);
            while(index < itemList.length){
                    item = itemList[index];                  
                    index += 1;   
                    const updated = await this.updateItem(ctx, item, "Allocated", ordNum); 
                }
         }
         else{
            logger.info('Users under the following MSP : '+
                        mspID + 'cannot perform this action');
            return('Users under the following MSP : '+
            mspID + 'cannot perform this action');
         }
    }

// The order is confirmed by PoTo
    async confirmOrder(ctx, ordNum) {

        let logger = shim.newLogger('Chaincode --> ');
        let CID = new shim.ClientIdentity(ctx.stub);
        let mspID = CID.getMSPID();
        logger.info('MSPID : '+ mspID);

         let item = "";
         let index = 0;
         
         const PoExists = await this.ordExists(ctx, ordNum);
         if (!PoExists) {
             throw new Error(`An open order with the Order number:${ordNum} doesnot exists`);
         }
        
         let orderBuffer = await ctx.stub.getState(ordNum);
         let ordDetail = JSON.parse(orderBuffer.toString()); 
         let poOwner = ordDetail.PoFromId;

         // Change Status of the Order *****
         if(ordDetail.PoToId == mspID){ 
            ordDetail.PoStatus = "Confirmed";
        
            const newBuffer = Buffer.from(JSON.stringify(ordDetail));
            await ctx.stub.putState(ordNum, newBuffer);
         }
         
         // Change Item Owner and Status ****
         if(ordDetail.PoToId == mspID){         
            const itemList = await this.getPoItems(ctx, ordNum);
            while(index < itemList.length){
                    item = itemList[index];                  
                    index += 1;   
                    const updated = await this.updateItemOwner(ctx, item, poOwner);
                }
         }
         else{
            logger.info('Users under the following MSP : '+
                        mspID + 'cannot perform this action');
            return('Users under the following MSP : '+
            mspID + 'cannot perform this action');
         }
    }

    async deleteOrd(ctx, ordNum) {
        
        const exists = await this.ordExists(ctx, ordNum);
        if(!exists) {
            throw new Error(`An open order with the Order number:${ordNum} doesnot exists`);
        }
        return await ctx.stub.deleteState(ordNum);
    }

    //QUERY ALL (Returns all item and orders)
    async queryAll(ctx) {

        const queryString = {
            selector: {

            }
        };
        let resultsIterator = await ctx.stub.getQueryResult(JSON.stringify(queryString));
        let results = await this.getAllResults(resultsIterator, false);

        return JSON.stringify(results);
    }

    // queryAllItems
    async queryAllItems(ctx) {

        const queryString = {
            selector: {
                AssetCategory: "Product",
            }
        };

        let resultsIterator = await ctx.stub.getQueryResult(JSON.stringify(queryString));
        let results = await this.getAllResults(resultsIterator, false);

        return JSON.stringify(results);
    }


    // queryAllOrders
    async queryAllOrders(ctx) {

        const queryString = {
            selector: {
                AssetCategory: "Order",
            }
        };
        
        let resultsIterator = await ctx.stub.getQueryResult(JSON.stringify(queryString));
        let results = await this.getAllResults(resultsIterator, false);

        return JSON.stringify(results);
    }

    async getAllResults(iterator, isHistory) {
        let allResults = [];

        while (true) {
            let res = await iterator.next();
            if (res.value && res.value.value.toString()) {
                let jsonRes = {};
                console.log(res.value.value.toString('utf8'));

                if (isHistory && isHistory === true) {
                    jsonRes.TxId = res.value.tx_id;
                    jsonRes.Timestamp = res.value.timestamp;
                    jsonRes.IsDelete = res.value.is_delete.toString();
                    try {
                        jsonRes.Value = JSON.parse(res.value.value.toString('utf8'));
                    } catch (err) {
                        console.log(err);
                        jsonRes.Value = res.value.value.toString('utf8');
                    }
                } else {
                    jsonRes.Key = res.value.key;
                    try {
                        jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
                    } catch (err) {
                        console.log(err);
                        jsonRes.Record = res.value.value.toString('utf8');
                    }
                }
                allResults.push(jsonRes);
            }
            if (res.done) {
                console.log('end of data');
                await iterator.close();
                console.info(allResults);
                return allResults;
            }
        }
    }

} //MyScmContract

module.exports = MyScmContract;