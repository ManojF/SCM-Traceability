var express = require('express');
var router = express.Router();
const {clientApplication} = require('./client')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('manufacturer', { title: 'Manufacturer Dash' });
});

router.get('/table', function(req, res, next) {
  res.render('table', { title: 'table' });
});
router.get('/event', function(req, res, next) {
  res.render('event', { title: 'Event' });
});

router.post('/manuwrite',function(req,res){

  const id    = req.body.itemID;
  const desc  = req.body.itemDesc;
  const um    = req.body.itemUM;
  const price = req.body.itemPrice;


  
  let ManufacturerClient = new clientApplication();
  ManufacturerClient.setRoleAndIdentity("manufacturer","ManAdmin")
  ManufacturerClient.initChannelAndChaincode("scmchannel", "mySCM")
  ManufacturerClient.generatedAndSubmitTxn(
      "createItem",
      id,desc,um,price
    )
    .then(message => {
      console.log(message);
    });
});

router.post('/manuread',async function(req,res){
  const id = req.body.itemID;;
  let ManufacturerClient = new clientApplication();
  ManufacturerClient.setRoleAndIdentity("manufacturer","ManAdmin")
  ManufacturerClient.initChannelAndChaincode("scmchannel", "mySCM")
  ManufacturerClient.generatedAndSubmitTxn("readItem", id).then(message => {
    console.log(message.toString());
    res.send({ Itemdata : message.toString() });
  });

 })



module.exports = router;


