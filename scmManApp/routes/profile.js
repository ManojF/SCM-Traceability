let profile = {
    distributor: {    
        "Wallet":"/home/manoj/kba/network/wallets/Distributor",
        "CCP": "/home/manoj/kba/network/gateways/Distributor/Distributor gateway.json"
    },
    
    retailer : {    
        "Wallet":"/home/manoj/kba/network/wallets/Retailer",
        "CCP": "/home/manoj/kba/network/gateways/Retailer/Retailer gateway.json"
    },
    
    manufacturer:{
        "Wallet":"/home/manoj/kba/network/wallets/Manufacturer",
        "CCP": "/home/manoj/kba/network/gateways/Manufacturer/Manufacturer gateway.json"
    }
    
}

module.exports = {
    profile
}
    